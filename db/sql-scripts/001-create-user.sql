CREATE USER 'realtimepricing'@'%' IDENTIFIED BY 'realtimepricing';
GRANT ALL PRIVILEGES ON *.* TO 'realtimepricing'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;